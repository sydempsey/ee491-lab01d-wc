///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file command-line-parser.c
/// @version 1.0
///
/// @author Sydney Dempsey <dempseys@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26-01-2021
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "command-line-parser.h"
#define EXIT_SUCCESS 0
#define SAME_STRING 0

int parseArgs(int numArgs, char *argv[]){

   //Too many inputs
   if(numArgs < 2 || numArgs > 3){
      fprintf(stderr, "Usage: wc [%s]\n", argv[1]);
      exit(EXIT_FAILURE);
   }

   // handles --version or [file] commands
   else if( numArgs == 2 ){

      if( (strcmp(argv[1], "--version")) == 0 )
            return Version;

      FILE *file = fopen(argv[1], "r");

      if(file == NULL){
         fprintf(stderr, "wc: Can't open [%s]\n", argv[1]);
         exit(EXIT_FAILURE);
      }

      return All;
   }


   //Specific wc command entered
   else if( numArgs == 3 ){

      if( (strcmp(argv[1], "-c")) == SAME_STRING || (strcmp(argv[1], "--bytes")) == SAME_STRING  )
            return Characters;

      else if( (strcmp(argv[1], "-l")) == SAME_STRING || (strcmp(argv[1], "--lines")) == SAME_STRING  )
            return Lines;
      
      else if( (strcmp(argv[1], "-w")) == SAME_STRING || (strcmp(argv[1], "--words")) == SAME_STRING  )
            return Words;
      
      else {
         fprintf(stderr, "wc: Unknown option '%s'\n", argv[1]);
         exit(EXIT_FAILURE);
      }
   }
   return EXIT_SUCCESS;
}
