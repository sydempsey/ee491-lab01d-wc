///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Sydney Dempsey <dempseys@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26-01-2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "wc.h"
#include "command-line-parser.h"

#define PROGRAM "wc"
#define VERSION "2.0"

int main(int numArgs, char *argv[]) {

   int command = parseArgs(numArgs, argv);

   if(command == All){
      getCounts(argv[1], command);
      return EXIT_SUCCESS;
   }

   else if(command == Version){
      printf("%s: Version %s\n", PROGRAM, VERSION);
   }

   else if(command == Characters || command == Lines || command == Words){
      getCounts(argv[2], command);
      return EXIT_SUCCESS;
   }

   else exit(EXIT_FAILURE);
}


void getCounts(char *file, int command){
   FILE *fptr = fopen(file, "r"); //open file in read mode
   int numLines = 0; int numChars = 0; int numWords = 0;
   int inWord = FALSE;
   int currChar;

   //if able to open file:
   if(fptr){

      //checks if end of file & gets new char
      while((currChar = fgetc(fptr)) != EOF){
         numChars ++;
         
         //adds word after white space encountered, checks and changes state
         if( WhiteSpace(currChar) ){
            if(inWord){
               inWord = FALSE;
               numWords++;
            }
         }

         //new line
         if(currChar == '\n' || currChar == '\0'){
            numLines ++;
         }

         //checks state
         else if( !WhiteSpace(currChar) ) inWord = TRUE;
      }

      
      //Print the requested values from the inputted command
      if(command == All){
         printf("%d \t %d \t %d \t [%s] \n\n", numLines, numWords, numChars, file);
      }

      else if(command == Words){
         printf("%d \t [%s] \n\n", numWords, file);
      }

      else if(command == Lines){
         printf("%d \t [%s] \n\n", numLines, file);
      }

      else if(command == Characters){
         printf("%d \t [%s] \n\n", numChars, file);
      }
   }

   else{ //unable to open file
      fprintf(stderr, "wc: Unable to open [%s]\n", file);
   }

   fclose(fptr);
}

int WhiteSpace(int currChar){
   
   if( (currChar == ' ') || (currChar == '\t') || (currChar == '\n') || (currChar == '\0') || (currChar == '\r') )
      return TRUE;
   else return FALSE;

}
