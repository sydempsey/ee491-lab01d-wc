# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall
OBJFILES = wc.o command-line-parser.o
TARGET = wc

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES)

test:
	$(TARGET)  test1.txt
	./wc --bytes test1.txt
	./wc test2.txt
	./wc -c test2.txt

clean:
	rm -f $(OBJFILES) $(TARGET) *~

