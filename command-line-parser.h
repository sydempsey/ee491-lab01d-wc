///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file command-line-parser.h
/// @version 1.0
///
/// @author Sydney Dempsey <dempseys@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26-01-2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

enum commands{All, Characters, Lines, Words, Version};
int parseArgs(int numArgs, char *argv[]);
