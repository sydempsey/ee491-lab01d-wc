///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.h
/// @version 1.0
///
/// @author Sydney Dempsey <dempseys@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   26-01-2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#define EXIT_SUCCESS  0
#define EXIT_FAILURE  1
#define TRUE 1
#define FALSE 0

void getCounts(char *, int command);
int WhiteSpace(int currChar);
